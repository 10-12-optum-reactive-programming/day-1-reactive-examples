package com.example.demo.reactor.core;

import java.util.function.Consumer;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class PublisherExample {
	
	public static void main(String[] args) {
		//System.out.println("Hello World !!");
		//Publisher
		Integer integer = 22;
		Mono<Integer> integerMono = Mono.just(22);
		
		//System.out.println("integer :: "+ integer);
		//System.out.println("integer-mono :: "+ integerMono);
		
		//Consumer<Integer> printConsumer = data -> System.out.println(data);
		//Method-reference
		/*
		 * Consumer<Integer> printConsumer = System.out::println;
		 * integerMono.subscribe(printConsumer);
		 */
		
		/*
		 * integerMono.subscribe(new Subscriber<Integer>() {
		 * 
		 * Subscription subscription = null; //This method will be called only once, the
		 * first time the consumer has created the subscription //This is a callback
		 * 
		 * @Override public void onSubscribe(Subscription subscription) {
		 * this.subscription = subscription; subscription.request(20); }
		 * 
		 * //This method will be called every time the data is published
		 * 
		 * @Override public void onNext(Integer data) {
		 * System.out.println("On Next method invoked with the data :: "+data); }
		 * 
		 * //This method will be called in case of an error.
		 * 
		 * @Override public void onError(Throwable t) {
		 * System.out.println("on Error method is invoked"); }
		 * 
		 * //This method will be called only once at the end of the producer producing
		 * the data
		 * 
		 * @Override public void onComplete() {
		 * System.out.println("On Complete method is invoked:: "); } });
		 */
		
		//Flux
		Flux<Integer> integerFlux = Flux.just(11, 22, 33, 44, 55, 66, 77, 88, 99);
		
		Consumer<Integer> printConsumer = data -> System.out.println(data);
		
		//integerFlux.subscribe(printConsumer);
		
		integerFlux.subscribe(new Subscriber<Integer>() {
			Subscription subscription = null;

			@Override
			public void onSubscribe(Subscription subscription) {
				subscription.request(Long.MAX_VALUE);
				this.subscription = subscription;
				
			}

			@Override
			public void onNext(Integer value) {
				System.out.println("data " + value);
				if(value == 44) {
					this.subscription.cancel();
				}
				//this.subscription.request(1);
				
			}

			@Override
			public void onError(Throwable t) {
				System.out.println("On error");
			}

			@Override
			public void onComplete() {
				System.out.println("On complete");
			}
		});
	}
}
