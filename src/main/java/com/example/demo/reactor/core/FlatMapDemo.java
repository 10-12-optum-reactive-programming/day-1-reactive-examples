package com.example.demo.reactor.core;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

import com.example.demo.model.LineItem;
import com.example.demo.model.Order;
import com.github.javafaker.Faker;

import reactor.core.publisher.Flux;

public class FlatMapDemo {
	
	public static void main(String[] args) {
		Flux<Order> orders = fetchAllOrders();
		
		/*
		 * orders.map(order -> order.getEmail()) .subscribe(email ->
		 * System.out.println(email));
		 * 
		 * orders.map(order -> order.getId()) .subscribe(email ->
		 * System.out.println(email));
		 */
		/*
		 * orders.map(order -> order.getLineItems()) .subscribe(email ->
		 * System.out.println(email));
		 */
		
		Flux<Set<LineItem>> fluxOfSetOfLineItems = orders.map(Order::getLineItems);
		Flux<LineItem> fluxOfLineItems = fluxOfSetOfLineItems.flatMap(values -> Flux.fromIterable(values));
		fluxOfLineItems.map(LineItem::getPrice).distinct().subscribe(System.out::println);
		
	}
	
	 private static Flux<Order> fetchAllOrders() {
	        Faker faker = new Faker();
	        Set<Order> orders = new HashSet<>();
	        IntStream.range(1, 4)
	                .forEach(index -> {
	                    Order order = Order.builder()
				                            .email(faker.internet().emailAddress())
				                            .id(faker.number().randomNumber())
				                            .lineItems(new HashSet<>())
				                            .build();
	                    IntStream.range(1,4).forEach(val -> {
	                        LineItem lineItem = LineItem.builder()
				                                .name(faker.commerce().productName())
				                                .price(faker.number().randomDouble(2, 10000, 20000))
				                                .qty(faker.number().numberBetween(2,5))
				                                .build();
	                        order.getLineItems().add(lineItem);
	                    });
	                    orders.add(order);
	                });
	        return Flux.fromIterable(orders);
	    }

}
