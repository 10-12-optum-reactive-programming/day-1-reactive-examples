package com.example.demo.publisher;

import static java.time.Duration.of;
import static java.time.temporal.ChronoUnit.SECONDS;
import static reactor.core.publisher.Flux.just;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class MonoTests {

	@Test
	public void testMonoPublisher() {
		Mono<String> monoValue = Mono.just("Reactive-programming");
		StepVerifier.create(monoValue).expectNext("Reactive-programming").expectComplete().verify();
	}
	
	@Test
	void testEmptyMono() {
		Mono<String> emptyMono = Mono.empty();
		StepVerifier.create(emptyMono).expectComplete().verify();
	}
	
	@Test
	void testFluxMany() {
		Flux<Integer> valueFlux = Flux.just(11, 22, 33, 44, 55);
		StepVerifier.create(valueFlux).expectNext(11,22,33,44,55).expectComplete().verify();
	}
	
	@Test
	void fluxArray() {
		Integer[] values = {11,22,33,44,55};
		Flux<Integer> valueFlux = Flux.fromArray(values);
		StepVerifier.create(valueFlux).expectNext(11,22,33,44,55).expectComplete().verify();
	}
	
	@Test
	void fluxFromList() {
		Flux<Integer> valueFlux = Flux.fromIterable(List.of(11,22,33,44,55));
		StepVerifier.create(valueFlux).expectNext(11,22,33,44,55).expectComplete().verify();
	}
	
	
	@Test
	void fluxFromStream() {
		Stream<Integer> streamOfIntegers = Stream.of(1,2,3,4,5,6,7,8);
		Flux<Integer> streamFlux = Flux.fromStream(streamOfIntegers);
		StepVerifier.create(streamFlux).expectNext(1,2,3,4,5,6,7,8).expectComplete().verify();
	}
	
	@Test
	void fluxFromRange() {
		Flux<Integer> streamFlux = Flux.range(1, 10);
		StepVerifier.create(streamFlux).expectNext(1,2,3,4,5,6,7,8,9,10).expectComplete().verify();
	}
	
	@Test
	void fluxWithException() {
		Flux<Integer> numberFlux = Flux.fromIterable(List.of(11, 22));
				
		numberFlux = numberFlux.concatWith(Flux.error(new IllegalArgumentException("invalid data")))	;	
		numberFlux = numberFlux.concatWithValues(33 , 44, 55);
		/*
		 * numberFlux.subscribe(new Subscriber<Integer>() {
		 * 
		 * Subscription subscription = null;
		 * 
		 * @Override public void onSubscribe(Subscription subscription) {
		 * this.subscription = subscription; this.subscription.request(Long.MAX_VALUE);
		 * 
		 * }
		 * 
		 * @Override public void onNext(Integer data) { System.out.println("data :: " +
		 * data); }
		 * 
		 * @Override public void onError(Throwable t) { System.out.println("Exception::"
		 * + t.getMessage());
		 * 
		 * }
		 * 
		 * @Override public void onComplete() {
		 * System.out.println("This will not be called::::");
		 * 
		 * } });
		 */
		StepVerifier.create(numberFlux).expectNext(11, 22).expectError(IllegalArgumentException.class).verify();
	}
	
	//@Test
	void fluxWithDelay() throws InterruptedException {
		Flux<String> namesFlux = just("hari", "vinod", "ramesh")
								.delayElements(of(2, SECONDS));
		namesFlux
			.log()
			.subscribe(System.out::println);
		Thread.sleep(50000);
	}
	
	@Test
	void subscribeFlux() {
		Flux<String> namesFlux = just("hari", "vinod", "ramesh").concatWith(Flux.error(() -> new IllegalArgumentException()));
		namesFlux.subscribe(
					//on-next
					System.out::println,
					//on-error
					(error) -> System.out.println(error.getMessage()),
					//on-complete
					() -> System.out.println("On complete")
				);
		StepVerifier.create(namesFlux).expectNext("hari", "vinod", "ramesh").expectError().verify();
	}
	
}
