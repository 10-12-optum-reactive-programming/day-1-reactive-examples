package com.example.demo;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class OperatorTest {

	private static Flux<Integer> valueFlux;

	@BeforeAll
	static void setup() {
		valueFlux = Flux.fromIterable(List.of(11, 22, 33, 44, 55));

	}

	@Test
	//one-to-one
	void testMapOperator() {

		Function<Integer, Integer> doubleMultiplier = (input) -> input * 2;

		Flux<Integer> doubleFlux = valueFlux.map(doubleMultiplier);

		StepVerifier.create(doubleFlux).expectNext(22, 44, 66, 88, 110).expectComplete().verify();

	}

	@Test
	//predicate = accepts object and returns a boolean
	void testFilterOperator() {
		
		//functional interface
		Predicate<Integer> evenFilter = (input) -> input % 2 == 0;

		Flux<Integer> evenFilterFlux = valueFlux.filter(evenFilter);
		Flux<Integer> oddFilterFlux = valueFlux.filter(evenFilter.negate());
		StepVerifier.create(evenFilterFlux).expectNext(22, 44).expectComplete().verify();
		StepVerifier.create(oddFilterFlux).expectNext(11, 33, 55).expectComplete().verify();
	}

	// @Test
	void mergeFluxOperator() throws InterruptedException {

		Predicate<Integer> evenFilter = (input) -> input % 2 == 0;
		Flux<Integer> evenFilterFlux = valueFlux.filter(evenFilter).delayElements(Duration.of(2, ChronoUnit.SECONDS));
		Flux<Integer> oddFilterFlux = valueFlux.filter(evenFilter.negate())
				.delayElements(Duration.of(3, ChronoUnit.SECONDS));

		Flux<Integer> mergeFlux = Flux.merge(evenFilterFlux, oddFilterFlux);

		mergeFlux.subscribe(data -> System.out.println(data));
		Thread.sleep(Long.MAX_VALUE);

	}

	// @Test
	void mergeFluxOperatorWithException() throws InterruptedException {

		Predicate<Integer> evenFilter = (input) -> input % 2 == 0;
		Flux<Integer> evenFilterFlux = valueFlux.filter(evenFilter)
				.concatWith(Flux.error(new IllegalArgumentException()));// .delayElements(Duration.of(2,
																		// ChronoUnit.SECONDS));
		Flux<Integer> oddFilterFlux = valueFlux.filter(evenFilter.negate());// .delayElements(Duration.of(3,
																			// ChronoUnit.SECONDS));

		Flux<Integer> mergeFlux = Flux.merge(evenFilterFlux, oddFilterFlux);
		// .onErrorReturn(200);

		mergeFlux.subscribe(data -> System.out.println(data));
		Thread.sleep(Long.MAX_VALUE);
	}

	// @Test
	void concatFluxOperator() throws InterruptedException {

		Predicate<Integer> evenFilter = (input) -> input % 2 == 0;
		Flux<Integer> evenFilterFlux = valueFlux.filter(evenFilter).delayElements(Duration.of(3, ChronoUnit.SECONDS));
		Flux<Integer> oddFilterFlux = valueFlux.filter(evenFilter.negate())
				.delayElements(Duration.of(1, ChronoUnit.SECONDS));

		Flux<Integer> mergeFlux = Flux.concat(evenFilterFlux, oddFilterFlux);
		// .onErrorReturn(200);
		//
		// evenFilterFlux.concatWith(oddFilterFlux);

		mergeFlux.subscribe(data -> System.out.println(data));
		Thread.sleep(Long.MAX_VALUE);

	}

	// @Test
	void zipOperator() throws InterruptedException {
		Flux<Integer> flux1 = Flux.just(23, 12, 234, 34, 45, 67, 1).delayElements(Duration.of(1, ChronoUnit.SECONDS));

		Flux<Integer> flux2 = Flux.just(66, 77, 46, 75, 63, 13).delayElements(Duration.of(2, ChronoUnit.SECONDS));

		// synchronous function
		BiFunction<Integer, Integer, Integer> minBiFunction = (val1, val2) -> Math.min(val1, val2);

		Flux<Integer> minValueFlux = flux1.zipWith(flux2, minBiFunction);
		// Flux<Integer> minValueFlux = Flux.zip(flux1, flux2, minBiFunction);

		minValueFlux.subscribe(data -> System.out.println(data));

		Thread.sleep(20_000);
	}

	@Test
	void flatMapOperator() {
		Flux<String> names = Flux.just("a-b-c", "c-d-e", "e-f-g", "g-h-i", "i-j-k");
		Flux<String> flattenedMap = names.flatMap(str -> Flux.fromArray(str.split("-")));
		flattenedMap.distinct().subscribe(System.out::print);
	}

	@Test
	public void testReduce() {
		Flux<Integer> ages = Flux.just(11, 22, 55, 66, 33, 99);
		// reduction opertators
		/*
		 * sum, max, avg, min, product
		 */
		BiFunction<Integer, Integer, Integer> max = (input1, input2) -> Integer.max(input1, input2);

		BiFunction<Integer, Integer, Integer> sum = (input1, input2) -> input1 + input2;

		// ages.reduce(max).subscribe(maxAge -> System.out.println(" Max age:: "+
		// maxAge));
		ages.reduce(sum).subscribe(total -> System.out.println(" Total age:: " + total));
	}

	// @Test
	public void concatMap() throws InterruptedException {
		Flux<String> firstNames = Flux.just("Harish", "Vishnu", "Ramesh");
		// Flux<String> citiesFlux = Flux.fromIterable(List.of("Mumbai", "Pune",
		// "Bangalore"));

		firstNames.concatMap(name -> Flux.just(name.split(""))).delayElements(Duration.ofMillis(200))
				.subscribe(res -> System.out.println(res));
		Thread.sleep(50_000);
	}

	// @Test
	public void testSwitchMap() throws InterruptedException {
		Flux<String> firstNames = Flux.just("Harish", "Vishnu", "Ramesh");
		Flux<Integer> empIds = Flux.just(1, 2, 3, 4, 5, 6, 7, 8);
		// empIds.switchMap(id -> getEmployeeDetails(id)).subscribe(res ->
		// System.out.println(res));
		firstNames.switchMap(name -> Flux.just(name.toUpperCase())).subscribe(response -> System.out.println(response));
		Thread.sleep(20_0000);
	}

	@Test
	public void testDefaultIfEmpty() {
		Flux<String> firstNames = Flux.just("Hasdsd", "Vsdasd", "Ramesh", "Kiran", "Manu");

		Function<Flux<String>, Flux<String>> filter = name -> name.filter(value -> value.length() < 3);

		Flux<String> responseFlux = firstNames.transform(filter).defaultIfEmpty("Default");

		responseFlux.subscribe(data -> System.out.println(data));
	}

	@Test
	public void testSwitchIfEmpty() {
		Flux<String> firstNames = Flux.just("Harish", "Vishnu", "Ramesh", "Kiran", "Manu");

		Function<Flux<String>, Flux<String>> filter = name -> name.filter(value -> value.length() < 3);

		Flux<String> responseFlux = firstNames.transform(filter)
				.switchIfEmpty(Flux.just("Shiva", "Ke", "Vinod").transform(filter));

		responseFlux.subscribe(data -> System.out.println(data));
	}
	
	@Test
	void testDoOnMethods() {
		Flux<String> firstNames = Flux.just("Harish", "Vishnu", "Ramesh", "Kiran", "Manu");

		Flux<String> fluxWithDoOnMethods = firstNames
												.filter(name -> name.length() < 5)
												.doOnNext(value -> System.out.println("on Next"+ value))
												.doOnSubscribe(sub -> System.out.println(sub))
												.doOnComplete(() -> System.out.println("on complete event triggered"));
		fluxWithDoOnMethods.subscribe();
	}
	
	@Test
	//to convert one type of exception to another type of exception
	void testErrorMap() {
		 Flux<Integer> dataFlux = Flux.fromIterable(List.of(11, 22, 33, 44, 55, 66));
		 Flux<Integer>invalidData = dataFlux
				 .map(val -> {
					 if(val == 33) {
						 throw new IllegalArgumentException("invalid data");
					 }
					 return val;
				 });
		 
		 invalidData.onErrorMap(throwable -> {
			 System.out.println(" exception while processing the record "+ throwable.getMessage());
			 return new IllegalArgumentException(throwable.getMessage());
		 })
		 .subscribe(val -> System.out.println(val));
	}
	
	@Test
	void onErrorContinue() {
		
		Flux<Integer> dataFlux = Flux.fromIterable(List.of(11, 22, 33, 44, 55, 66));
		 Flux<Integer>invalidData = dataFlux
				 .map(val -> {
					 if(val == 33) {
						 throw new IllegalArgumentException("invalid data");
					 }
					 return val;
				 });
		 
		 invalidData.onErrorContinue((throwable, value) -> {
			 System.out.println("exception while processing the record :: "+value +" ::"+ throwable.getMessage());
			
		 })
		 .subscribe(val -> System.out.println(val));
	}
	
	 @Test
	    public void testErrorReturn(){
	        Flux<Integer> invalidData = Flux.fromIterable(List.of(11, 22, 33, 44, 55, 66))
	                .map(val -> {
	                    if (val == 33) {
	                        throw new IllegalArgumentException("invalid data");
	                    }
	                    return val;
	                })
	                .onErrorReturn(38);
	        invalidData.subscribe(val -> System.out.println(val), (error) -> System.out.println("error"+ error), ()-> System.out.println("on completion") );
	    }
}
