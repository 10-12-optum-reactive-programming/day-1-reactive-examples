package com.example.demo;

import java.time.Duration;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

public class PublisherDemo {

	// creating the flux
	@Test
	void testFluxWithCreate() throws InterruptedException {
		Flux<Integer> integerFlux = Flux.create((FluxSink<Integer> sink) -> {
			IntStream.range(1, 1000).forEach(index -> {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Generating the next element " + index + " " + Thread.currentThread().getName());
				sink.next(index * 4);
			});
		});
		
		integerFlux.subscribe(consumer -> System.out.println(consumer));
		Thread.sleep(20_000);
	}
	
	@Test
	void testFluxWithGenerator() {
		Flux<String> flux = Flux.generate(() -> 1, (state, sink) -> {
			System.out.println("Emmitting the event ::: "+ state);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sink.next("state :: "+ state * 3);
			return state + 1;
		});
		
		flux.subscribe(event -> System.out.println(event));
	}
	
	
	@Test
	void coldPublisher() throws InterruptedException {
		Flux<Integer> coldFlux = Flux.range(1, 10).delayElements(Duration.ofMillis(100));
		
		coldFlux.subscribe(value -> System.out.println("subscriber - 1:: "+ value));
		coldFlux.subscribe(value -> System.out.println("subscriber - 2:: "+ value));
		coldFlux.subscribe(value -> System.out.println("subscriber - 3:: "+ value));
		coldFlux.subscribe(value -> System.out.println("subscriber - 4:: "+ value));
		Thread.sleep(40000);
	}
	
	@Test
	void hotPublisher() throws InterruptedException {
		Flux<Integer> coldFlux = Flux.range(1, 10).delayElements(Duration.ofSeconds(2));
		ConnectableFlux<Integer> hotPublisher = coldFlux.publish();
		
		//explicitly triggering the hot publisher
		hotPublisher.connect();
		hotPublisher.subscribe(value -> System.out.println("Subscriber - 1:  " + value));
		
		Thread.sleep(2000);
		
		hotPublisher.subscribe(value -> System.out.println("Subscriber - 2: " + value));
		
		Thread.sleep(2000);
		
		hotPublisher.subscribe(value -> System.out.println("Subscriber - 3: " + value));
		
		Thread.sleep(40_000);
	}
	
	@Test
	void hotPublisherAutoConnect() throws InterruptedException {
		Flux<Integer> coldFlux = Flux.range(1, 10).delayElements(Duration.ofSeconds(2));
		Flux<Integer> hotPublisher = coldFlux.publish().autoConnect();
		
		hotPublisher.subscribe(value -> System.out.println("Subscriber - 1:  " + value));
		
		Thread.sleep(2000);
		
		hotPublisher.subscribe(value -> System.out.println("Subscriber - 2: " + value));
		Thread.sleep(2000);
		hotPublisher.subscribe(value -> System.out.println("Subscriber - 3: " + value));
		Thread.sleep(40_000);
	}
	
	@Test
	void testRetry() throws InterruptedException {
		Flux<Integer> fetchUserAges = Flux.just(22,33,44,55,66, 11).delayElements(Duration.ofMillis(2000));
		
		Flux<String> retryFlux = fetchUserAges.map(age -> validateAgeForCastingVote(age))
					 .doOnError(err -> System.out.println("Error :: "+ err.getMessage()))
					 .retry(2);
		retryFlux.subscribe(System.out::println);
		
		Thread.sleep(60000);
					 
					 
	}

	private String validateAgeForCastingVote(Integer age) {
		if(age < 18) {
			throw new IllegalArgumentException("invalid age for casting vote:: "+ age);
		} 
		return "Valid:: "+ age;
	}
	
	


}
